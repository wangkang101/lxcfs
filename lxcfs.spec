#Global macro or variable
%define _unpackaged_files_terminate_build 0

#Basic Information
Name:      lxcfs
Version:   4.0.5
Release:   1
Summary:   FUSE filesystem for LXC
License:   Apache-2.0
URL:       http://linuxcontainers.org
Source:    https://linuxcontainers.org/downloads/%{name}/%{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-build
Patch9001: 0001-systemd.patch
Patch9002: 0002-remove-sysvinit-upstart.patch
Patch9003: 0003-show-dev-name-in-container.patch
Patch9004: 0004-lxcfs-fix-cpuinfo-print.patch
Patch9005: 0005-fix-memory-leak.patch
Patch9006: 0006-fix-concurrency-problem.patch
Patch9007: 0007-set-null-after-free.patch
Patch9008: 0008-fix-hang.patch
Patch9009: 0009-limit-stat-by-quota-period-setting.patch
Patch9010: 0010-diskstats-support-devicemapper-device.patch
Patch9011: 0011-lxcfs-add-proc-partitions.patch
Patch9012: 0012-lxcfs-proc_diskstats_read-func-obtain-data-from-blki.patch
Patch9013: 0013-add-secure-compile-option-in-Makefile.patch
Patch9014: 0014-fix-proc-diskstats-show-in-container.patch
Patch9015: 0015-lxcfs-adapt-4.18-kernel.patch

#Dependency
BuildRequires: autoconf automake libtool help2man
BuildRequires: fuse-devel libattr-devel

%description
lxcfs is a simple userspace filesystem designed to make containers
feel more like a real independent system through. It is usable by
any runtime, written in C using libfuse and glib.

%prep
%autosetup -p1

%build
autoreconf --force --install
%configure
make tests
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot} %{?_smp_mflags}
install -d -m 0755 %{buildroot}%{_unitdir}
install -p -m 0644 config/init/systemd/*.service %{buildroot}%{_unitdir}/
install -d -m 0755 %{buildroot}%{_sbindir}
ln -sf service %{buildroot}%{_sbindir}/rc%{name}
install -d -m 0755 %{buildroot}%{_includedir}/%{name}
install -d -m 0755 %{buildroot}%{_localstatedir}/lib/lxc/%{name}/

#Install and uninstall scripts
%pre

%preun

%post
ret=`systemctl is-active lxcfs`
if [ "$ret" == "active" ]; then
	systemctl restart lxcfs
fi

%postun

#Files list
%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog README
%license COPYING
%{_sbindir}/*
%{_bindir}/*
%{_datadir}/lxc
%{_datadir}/%{name}
%dir %{_localstatedir}/lib/lxc/%{name}
%dir %{_libdir}/%{name}
%{_libdir}/%{name}/lib%{name}.so
%exclude %{_libdir}/%{name}/lib%{name}.la
%{_mandir}/man1/*
%{_unitdir}/*

%changelog
* Fri Aug 28 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.0.5-1 
- update to 4.0.5

* Thu July 02 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.0.2-0.12
- Sync from internal changes

* Thu Dec 26 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.0.2-0.5.h2
- Remove securec functions

* Wed Sep 18 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.0.2-0.5.h1
- Package init
