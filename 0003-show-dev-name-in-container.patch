diff --git a/src/proc_fuse.c b/src/proc_fuse.c
index a99162c..b4f4129 100644
--- a/src/proc_fuse.c
+++ b/src/proc_fuse.c
@@ -37,6 +37,7 @@
 #include <sys/socket.h>
 #include <sys/syscall.h>
 #include <sys/sysinfo.h>
+#include <sys/sysmacros.h>
 #include <sys/vfs.h>
 
 #include "bindings.h"
@@ -374,6 +375,132 @@ static void get_blkio_io_value(char *str, unsigned major, unsigned minor,
 	}
 }
 
+struct devinfo {
+	char *name;
+	int major, minor;
+	struct devinfo *next;
+};
+
+int getns(pid_t pid, const char *ns_type)
+{
+	char fpath[100];
+	memset(fpath, 0, sizeof(fpath));
+	snprintf(fpath, 99, "/proc/%d/ns/%s", pid, ns_type);
+	return open(fpath, O_RDONLY);
+}
+
+struct devinfo* container_dev_read(pid_t pid) {
+	int nsfd;
+	DIR *dir;
+	struct dirent *ptr;
+	struct stat dev_stat;
+	struct devinfo *head = NULL, *end;
+	char fpath[100], dev_name[100];
+	pid_t child_pid;
+	int mypipe[2];
+	int dev_num;
+	FILE *stream;
+
+	if (pipe(mypipe) < 0) {
+		perror("Error creating pipe");
+		return head;
+	}
+
+	child_pid = fork();
+	if (child_pid < 0) {
+		close(mypipe[0]);
+		close(mypipe[1]);
+		perror("Error forking child");
+		return head;
+	}
+	if (child_pid == 0) {
+		close(mypipe[0]);
+		stream = fdopen(mypipe[1], "w");
+		if (stream == NULL) {
+			lxcfs_error("Error opening pipe for writing: %s\n", strerror(errno));
+			goto child_out;
+		}
+		nsfd = getns(pid, "mnt");
+		if (nsfd < 0) {
+			lxcfs_error("Error getting mnt ns: %s\n", strerror(errno));
+			goto child_out;
+		}
+		if (setns(nsfd, 0) < 0) {
+			lxcfs_error("Error setting mnt ns: %s\n", strerror(errno));
+			goto child_out;
+		}
+		dir = opendir("/dev");
+		if (dir == NULL) {
+			lxcfs_error("Error opening dir /dev: %s\n", strerror(errno));
+			goto child_out;
+		}
+		while ((ptr = readdir(dir)) != NULL) {
+			if (ptr->d_type != DT_BLK && ptr->d_type != DT_CHR) {
+				continue;
+			}
+			memset(fpath, 0, sizeof(fpath));
+			snprintf(fpath, 99, "/dev/%s", ptr->d_name);
+			stat(fpath, &dev_stat);
+			fprintf(stream, "%s %d ", ptr->d_name, dev_stat.st_rdev);
+			fflush(stream);
+		}
+		closedir(dir);
+		stat("/", &dev_stat);
+		dev_num = dev_stat.st_dev & (~0xf);
+		fprintf(stream, "sda %d end 0 ", dev_num);
+		fflush(stream);
+		fclose(stream);
+		exit(0);
+	}
+
+child_out:
+	close(mypipe[1]);
+	stream = fdopen(mypipe[0], "r");
+	if (stream == NULL) {
+		lxcfs_error("Error opening pipe for reading: %s\n", strerror(errno));
+		goto err;
+	}
+	while (fscanf(stream, "%100s%d", dev_name, &dev_num) == 2) {
+			if (dev_num == 0) {
+				break;
+			}
+			if (head == NULL) {
+				do {
+					head = (struct devinfo*)malloc(sizeof(struct devinfo));
+				} while (!head);
+				end = head;
+			} else {
+				do {
+					end->next  = (struct devinfo*)malloc(sizeof(struct devinfo));
+				} while (!end->next);
+				end = end->next;
+			}
+			end->next = NULL;
+			end->name = must_copy_string(dev_name);
+			end->major = major(dev_num);
+			end->minor = minor(dev_num);
+	}
+err:
+    if (stream)
+        fclose(stream);
+	if (child_pid > 0)
+		wait_for_pid(child_pid);
+	return head;
+}
+
+void free_devinfo_list(struct devinfo *ptr)
+{
+	struct devinfo *tmp;
+	while (ptr != NULL) {
+		tmp = ptr;
+		ptr = ptr->next;
+		free(tmp->name);
+		tmp->name = NULL;
+		free(tmp);
+		tmp = NULL;
+	}
+}
+
 static int proc_diskstats_read(char *buf, size_t size, off_t offset,
 			       struct fuse_file_info *fi)
 {
@@ -385,6 +512,7 @@ static int proc_diskstats_read(char *buf, size_t size, off_t offset,
 	__do_fclose FILE *f = NULL;
 	struct fuse_context *fc = fuse_get_context();
 	struct file_info *d = INTTYPE_TO_PTR(fi->fh);
+	struct devinfo *container_devinfo = NULL, *ptr = NULL;
 	uint64_t read = 0, write = 0;
 	uint64_t read_merged = 0, write_merged = 0;
 	uint64_t read_sectors = 0, write_sectors = 0;
@@ -458,13 +586,22 @@ static int proc_diskstats_read(char *buf, size_t size, off_t offset,
 	if (!f)
 		return 0;
 
+	container_devinfo = container_dev_read(initpid);
+
 	while (getline(&line, &linelen, f) != -1) {
 		ssize_t l;
 		char lbuf[256];
 
+		memset(dev_name, 0, sizeof(dev_name));
 		i = sscanf(line, "%u %u %71s", &major, &minor, dev_name);
 		if (i != 3)
 			continue;
+		for (ptr = container_devinfo; ptr != NULL; ptr = ptr->next) {
+			if (major == ptr->major && minor == ptr->minor) {
+				snprintf(dev_name, sizeof(dev_name), "%s", ptr->name);
+				dev_name[71] = '\0';
+			}
+		}
 
 		get_blkio_io_value(io_serviced_str, major, minor, "Read", &read);
 		get_blkio_io_value(io_serviced_str, major, minor, "Write", &write);
@@ -499,10 +636,14 @@ static int proc_diskstats_read(char *buf, size_t size, off_t offset,
 			continue;
 
 		l = snprintf(cache, cache_size, "%s", lbuf);
-		if (l < 0)
+		if (l < 0) {
+			free_devinfo_list(container_devinfo);
 			return log_error(0, "Failed to write cache");
-		if (l >= cache_size)
+		}
+		if (l >= cache_size) {
+			free_devinfo_list(container_devinfo);
 			return log_error(0, "Write to cache was truncated");
+		}
 
 		cache += l;
 		cache_size -= l;
